package solid;

import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {

		System.out.println("Hello world!");
		Pattern pat = (Pattern) new Principle();
		Principle pr = new Pattern();
		Principle[] prs = new Principle[5];
		Pattern[] pats = new Pattern[5];
		prs = pats;

		// With this prlist, assigning to a pattern list does not work
		//patList = prList
		//List< Principle> prList = new ArrayList<Principle>();

		// we have to use ? extends Principle
		List<? extends Principle> prList = new ArrayList<Principle>();
		List<Pattern> patList = new ArrayList<>();
		patList.add(new Pattern());
		prList = patList;
		Pattern patget = (Pattern) prList.get(0);
		Principle princeGet =  prList.get(0);
		prList.get(0).examine();
		// I cannot add a principle  to the list because the list really
		// is a list of patterns
		// This is a producer list
		//prList.add(new Principle());

		List <? super Pattern> superPattern = new ArrayList<Principle>();
		superPattern.add(new Pattern());
		// I can not add a Principle, because this might be a list of patterns
		//superPattern.add(new Principle());

		// I cannot get a pattern, because there are principles in here
		// Pattern gotPat = superPattern.get(0);


		Object gotIt =superPattern.get(0);
		Principle castPrinciple = (Principle)superPattern.get(0);





	}
}